FROM openjdk:8-alpine
COPY target/feign*.jar feign.jar
EXPOSE 8081
CMD ["java", "-jar", "feign.jar"]
