package kz.cloud.test.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author yerzhan
 */
@FeignClient("config-client")
public interface ConfigClient {

    @GetMapping("/")
    String hello();
}
