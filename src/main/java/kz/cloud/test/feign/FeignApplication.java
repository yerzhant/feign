package kz.cloud.test.feign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@EnableFeignClients
@RestController
@SpringBootApplication
public class FeignApplication {

    @Autowired
    private ConfigClient configClient;

	public static void main(String[] args) {
		SpringApplication.run(FeignApplication.class, args);
    }

    @GetMapping("/")
    public String hi() {
        return configClient.hello();
    }
}
